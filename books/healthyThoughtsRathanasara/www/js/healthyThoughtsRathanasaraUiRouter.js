(function() {
    'use strict';
    
mainApp.config(healthyThoughtsRathanasaraUiRouter);

//The name cannot have dash character.
function healthyThoughtsRathanasaraUiRouter($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('healthyThoughtsRathanasara', {
            url: '/healthyThoughtsRathanasara',
            views: {
              // templateUrl will be loaded in the un-named view in index.html.
              '': {
                templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara.html',
                //controller : 'bookCtrl'
              },
              
              // templateUrl will be loaded in the un-named view in bookFolder.html 
              // which is embedded in healthyThoughtsRathanasara.html. The book-folder  
              // directive in healthyThoughtsRathanasara.html will load bookFolder.html.
              // 
              // '@home' - if in mainUiRouter.
              // '@healthyThoughtsRathanasara' - if in healthyThoughtsRathanasaraUiRouter.
              '@healthyThoughtsRathanasara': {
                templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0001.html',
                //controller : 'bookCtrl'  
              }
            }
        })

        // templateUrl in the states listed below will be loaded in the 
        // un-named view in bookFolder.html         
        .state('healthyThoughtsRathanasara.0001', {
            url: '/healthyThoughtsRathanasara-0001',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0001.html'
        })
        .state('healthyThoughtsRathanasara.0002', {
            url: '/healthyThoughtsRathanasara-0002',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0002.html'
        })
        .state('healthyThoughtsRathanasara.0003', {
            url: '/healthyThoughtsRathanasara-0003',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0003.html'
        })
        .state('healthyThoughtsRathanasara.0004', {
            url: '/healthyThoughtsRathanasara-0004',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0004.html'
        })
        .state('healthyThoughtsRathanasara.0005', {
            url: '/healthyThoughtsRathanasara-0005',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0005.html'
        })
        .state('healthyThoughtsRathanasara.0006', {
            url: '/healthyThoughtsRathanasara-0006',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0006.html'
        })
        .state('healthyThoughtsRathanasara.0007', {
            url: '/healthyThoughtsRathanasara-0007',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0007.html'
        })
        .state('healthyThoughtsRathanasara.0008', {
            url: '/healthyThoughtsRathanasara-0008',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0008.html'
        })
        .state('healthyThoughtsRathanasara.0009', {
            url: '/healthyThoughtsRathanasara-0009',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0009.html'
        })
        .state('healthyThoughtsRathanasara.0010', {
            url: '/healthyThoughtsRathanasara-0010',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0010.html'
        })
        .state('healthyThoughtsRathanasara.0011', {
            url: '/healthyThoughtsRathanasara-0011',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0011.html'
        })
        .state('healthyThoughtsRathanasara.0012', {
            url: '/healthyThoughtsRathanasara-0012',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0012.html'
        })
        .state('healthyThoughtsRathanasara.0013', {
            url: '/healthyThoughtsRathanasara-0013',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0013.html'
        })
        .state('healthyThoughtsRathanasara.0014', {
            url: '/healthyThoughtsRathanasara-0014',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0014.html'
        })
        .state('healthyThoughtsRathanasara.0015', {
            url: '/healthyThoughtsRathanasara-0015',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0015.html'
        })
        .state('healthyThoughtsRathanasara.0016', {
            url: '/healthyThoughtsRathanasara-0016',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0016.html'
        })
        .state('healthyThoughtsRathanasara.0017', {
            url: '/healthyThoughtsRathanasara-0017',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0017.html'
        })
        .state('healthyThoughtsRathanasara.0018', {
            url: '/healthyThoughtsRathanasara-0018',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0012.html'
        })
        .state('healthyThoughtsRathanasara.0019', {
            url: '/healthyThoughtsRathanasara-0019',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0019.html'
        })
        .state('healthyThoughtsRathanasara.0020', {
            url: '/healthyThoughtsRathanasara-0020',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0020.html'
        })
        .state('healthyThoughtsRathanasara.0021', {
            url: '/healthyThoughtsRathanasara-0021',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0021.html'
        })
        .state('healthyThoughtsRathanasara.0022', {
            url: '/healthyThoughtsRathanasara-0022',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0022.html'
        })
        .state('healthyThoughtsRathanasara.0023', {
            url: '/healthyThoughtsRathanasara-0023',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0023.html'
        })
        .state('healthyThoughtsRathanasara.0024', {
            url: '/healthyThoughtsRathanasara-0024',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0024.html'
        })
        .state('healthyThoughtsRathanasara.0025', {
            url: '/healthyThoughtsRathanasara-0025',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0025.html'
        })
        .state('healthyThoughtsRathanasara.0026', {
            url: '/healthyThoughtsRathanasara-0026',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0026.html'
        })
        .state('healthyThoughtsRathanasara.0027', {
            url: '/healthyThoughtsRathanasara-0027',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0027.html'
        })
        .state('healthyThoughtsRathanasara.0028', {
            url: '/healthyThoughtsRathanasara-0028',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0028.html'
        })
        .state('healthyThoughtsRathanasara.0029', {
            url: '/healthyThoughtsRathanasara-0029',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0029.html'
        })
        .state('healthyThoughtsRathanasara.0030', {
            url: '/healthyThoughtsRathanasara-0030',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0030.html'
        })
        .state('healthyThoughtsRathanasara.0031', {
            url: '/healthyThoughtsRathanasara-0031',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0031.html'
        })
        .state('healthyThoughtsRathanasara.0032', {
            url: '/healthyThoughtsRathanasara-0032',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0032.html'
        })
        .state('healthyThoughtsRathanasara.0033', {
            url: '/healthyThoughtsRathanasara-0033',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0033.html'
        })
        .state('healthyThoughtsRathanasara.0034', {
            url: '/healthyThoughtsRathanasara-0034',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0034.html'
        })
        .state('healthyThoughtsRathanasara.0035', {
            url: '/healthyThoughtsRathanasara-0035',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0035.html'
        })
        .state('healthyThoughtsRathanasara.0036', {
            url: '/healthyThoughtsRathanasara-0036',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0036.html'
        })
        .state('healthyThoughtsRathanasara.0037', {
            url: '/healthyThoughtsRathanasara-0037',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0037.html'
        })
        .state('healthyThoughtsRathanasara.0038', {
            url: '/healthyThoughtsRathanasara-0038',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0038.html'
        })
        .state('healthyThoughtsRathanasara.0039', {
            url: '/healthyThoughtsRathanasara-0039',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0039.html'
        }) 
        .state('healthyThoughtsRathanasara.0040', {
            url: '/healthyThoughtsRathanasara-0040',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0040.html'
        })
        .state('healthyThoughtsRathanasara.0041', {
            url: '/healthyThoughtsRathanasara-0041',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0041.html'
        })
        .state('healthyThoughtsRathanasara.0042', {
            url: '/healthyThoughtsRathanasara-0042',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0042.html'
        })
        .state('healthyThoughtsRathanasara.0043', {
            url: '/healthyThoughtsRathanasara-0043',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0043.html'
        })
        .state('healthyThoughtsRathanasara.0044', {
            url: '/healthyThoughtsRathanasara-0044',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0044.html'
        })
        .state('healthyThoughtsRathanasara.0045', {
            url: '/healthyThoughtsRathanasara-0045',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0045.html'
        })
        .state('healthyThoughtsRathanasara.0046', {
            url: '/healthyThoughtsRathanasara-0046',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0046.html'
        })
        .state('healthyThoughtsRathanasara.0047', {
            url: '/healthyThoughtsRathanasara-0047',
            templateUrl: 'books/healthyThoughtsRathanasara/www/contents/healthyThoughtsRathanasara-0047.html'
        })

};


})();