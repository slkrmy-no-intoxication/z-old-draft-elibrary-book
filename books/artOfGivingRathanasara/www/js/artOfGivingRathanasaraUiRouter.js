(function() {
    'use strict';
    
mainApp.config(artOfGivingRathanasaraUiRouter);

//The name cannot have dash character.
function artOfGivingRathanasaraUiRouter($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('artOfGivingRathanasara', {
            url: '/artOfGivingRathanasara',
            views: {
              // templateUrl will be loaded in the un-named view in index.html.
              '': {
                templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara.html',
                //controller : 'bookCtrl'
              },
              
              // templateUrl will be loaded in the un-named view in bookFolder.html 
              // which is embedded in artOfGivingRathanasara.html. The book-folder  
              // directive in artOfGivingRathanasara.html will load bookFolder.html.
              // 
              // '@home' - if in mainUiRouter.
              // '@artOfGivingRathanasara' - if in artOfGivingRathanasaraUiRouter.
              '@artOfGivingRathanasara': {
                templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0001.html',
                //controller : 'bookCtrl'  
              }
            }
        })

        // templateUrl in the states listed below will be loaded in the 
        // un-named view in bookFolder.html         
        .state('artOfGivingRathanasara.0001', {
            url: '/artOfGivingRathanasara-0001',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0001.html'
        })
        .state('artOfGivingRathanasara.0002', {
            url: '/artOfGivingRathanasara-0002',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0002.html'
        })
        .state('artOfGivingRathanasara.0003', {
            url: '/artOfGivingRathanasara-0003',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0003.html'
        })
        .state('artOfGivingRathanasara.0004', {
            url: '/artOfGivingRathanasara-0004',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0004.html'
        })
        .state('artOfGivingRathanasara.0005', {
            url: '/artOfGivingRathanasara-0005',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0005.html'
        })
        .state('artOfGivingRathanasara.0006', {
            url: '/artOfGivingRathanasara-0006',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0006.html'
        })
        .state('artOfGivingRathanasara.0007', {
            url: '/artOfGivingRathanasara-0007',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0007.html'
        })
        .state('artOfGivingRathanasara.0008', {
            url: '/artOfGivingRathanasara-0008',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0008.html'
        })
        .state('artOfGivingRathanasara.0009', {
            url: '/artOfGivingRathanasara-0009',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0009.html'
        })
        .state('artOfGivingRathanasara.0010', {
            url: '/artOfGivingRathanasara-0010',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0010.html'
        })
        .state('artOfGivingRathanasara.0011', {
            url: '/artOfGivingRathanasara-0011',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0011.html'
        })
        .state('artOfGivingRathanasara.0012', {
            url: '/artOfGivingRathanasara-0012',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0012.html'
        })
        .state('artOfGivingRathanasara.0013', {
            url: '/artOfGivingRathanasara-0013',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0013.html'
        })
        .state('artOfGivingRathanasara.0014', {
            url: '/artOfGivingRathanasara-0014',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0014.html'
        })
        .state('artOfGivingRathanasara.0015', {
            url: '/artOfGivingRathanasara-0015',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0015.html'
        })
        .state('artOfGivingRathanasara.0016', {
            url: '/artOfGivingRathanasara-0016',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0016.html'
        })
        .state('artOfGivingRathanasara.0017', {
            url: '/artOfGivingRathanasara-0017',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0017.html'
        })
        .state('artOfGivingRathanasara.0018', {
            url: '/artOfGivingRathanasara-0018',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0012.html'
        })
        .state('artOfGivingRathanasara.0019', {
            url: '/artOfGivingRathanasara-0019',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0019.html'
        })
        .state('artOfGivingRathanasara.0020', {
            url: '/artOfGivingRathanasara-0020',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0020.html'
        })
        .state('artOfGivingRathanasara.0021', {
            url: '/artOfGivingRathanasara-0021',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0021.html'
        })
        .state('artOfGivingRathanasara.0022', {
            url: '/artOfGivingRathanasara-0022',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0022.html'
        })
        .state('artOfGivingRathanasara.0023', {
            url: '/artOfGivingRathanasara-0023',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0023.html'
        })
        .state('artOfGivingRathanasara.0024', {
            url: '/artOfGivingRathanasara-0024',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0024.html'
        })
        .state('artOfGivingRathanasara.0025', {
            url: '/artOfGivingRathanasara-0025',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0025.html'
        })
        .state('artOfGivingRathanasara.0026', {
            url: '/artOfGivingRathanasara-0026',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0026.html'
        })
        .state('artOfGivingRathanasara.0027', {
            url: '/artOfGivingRathanasara-0027',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0027.html'
        })
        .state('artOfGivingRathanasara.0028', {
            url: '/artOfGivingRathanasara-0028',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0028.html'
        })
        .state('artOfGivingRathanasara.0029', {
            url: '/artOfGivingRathanasara-0029',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0029.html'
        })
        .state('artOfGivingRathanasara.0030', {
            url: '/artOfGivingRathanasara-0030',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0030.html'
        })
        .state('artOfGivingRathanasara.0031', {
            url: '/artOfGivingRathanasara-0031',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0031.html'
        })
        .state('artOfGivingRathanasara.0032', {
            url: '/artOfGivingRathanasara-0032',
            templateUrl: 'books/artOfGivingRathanasara/www/contents/artOfGivingRathanasara-0032.html'
        })
};


})();