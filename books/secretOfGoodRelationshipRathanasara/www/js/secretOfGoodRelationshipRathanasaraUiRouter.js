(function() {
    'use strict';
    
mainApp.config(secretOfGoodRelationshipRathanasaraUiRouter);

//The name cannot have dash character.
function secretOfGoodRelationshipRathanasaraUiRouter($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('secretOfGoodRelationshipRathanasara', {
            url: '/secretOfGoodRelationshipRathanasara',
            views: {
              // templateUrl will be loaded in the un-named view in index.html.
              '': {
                templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara.html',
                //controller : 'bookCtrl'
              },
              
              // templateUrl will be loaded in the un-named view in bookFolder.html 
              // which is embedded in secretOfGoodRelationshipRathanasara.html. The book-folder  
              // directive in secretOfGoodRelationshipRathanasara.html will load bookFolder.html.
              // 
              // '@home' - if in mainUiRouter.
              // '@secretOfGoodRelationshipRathanasara' - if in secretOfGoodRelationshipRathanasaraUiRouter.
              '@secretOfGoodRelationshipRathanasara': {
                templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0001.html',
                //controller : 'bookCtrl'  
              }
            }
        })

        // templateUrl in the states listed below will be loaded in the 
        // un-named view in bookFolder.html         
        .state('secretOfGoodRelationshipRathanasara.0001', {
            url: '/secretOfGoodRelationshipRathanasara-0001',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0001.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0002', {
            url: '/secretOfGoodRelationshipRathanasara-0002',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0002.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0003', {
            url: '/secretOfGoodRelationshipRathanasara-0003',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0003.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0004', {
            url: '/secretOfGoodRelationshipRathanasara-0004',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0004.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0005', {
            url: '/secretOfGoodRelationshipRathanasara-0005',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0005.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0006', {
            url: '/secretOfGoodRelationshipRathanasara-0006',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0006.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0007', {
            url: '/secretOfGoodRelationshipRathanasara-0007',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0007.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0008', {
            url: '/secretOfGoodRelationshipRathanasara-0008',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0008.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0009', {
            url: '/secretOfGoodRelationshipRathanasara-0009',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0009.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0010', {
            url: '/secretOfGoodRelationshipRathanasara-0010',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0010.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0011', {
            url: '/secretOfGoodRelationshipRathanasara-0011',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0011.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0012', {
            url: '/secretOfGoodRelationshipRathanasara-0012',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0012.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0013', {
            url: '/secretOfGoodRelationshipRathanasara-0013',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0013.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0014', {
            url: '/secretOfGoodRelationshipRathanasara-0014',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0014.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0015', {
            url: '/secretOfGoodRelationshipRathanasara-0015',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0015.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0016', {
            url: '/secretOfGoodRelationshipRathanasara-0016',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0016.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0017', {
            url: '/secretOfGoodRelationshipRathanasara-0017',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0017.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0018', {
            url: '/secretOfGoodRelationshipRathanasara-0018',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0012.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0019', {
            url: '/secretOfGoodRelationshipRathanasara-0019',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0019.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0020', {
            url: '/secretOfGoodRelationshipRathanasara-0020',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0020.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0021', {
            url: '/secretOfGoodRelationshipRathanasara-0021',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0021.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0022', {
            url: '/secretOfGoodRelationshipRathanasara-0022',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0022.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0023', {
            url: '/secretOfGoodRelationshipRathanasara-0023',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0023.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0024', {
            url: '/secretOfGoodRelationshipRathanasara-0024',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0024.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0025', {
            url: '/secretOfGoodRelationshipRathanasara-0025',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0025.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0026', {
            url: '/secretOfGoodRelationshipRathanasara-0026',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0026.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0027', {
            url: '/secretOfGoodRelationshipRathanasara-0027',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0027.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0028', {
            url: '/secretOfGoodRelationshipRathanasara-0028',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0028.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0029', {
            url: '/secretOfGoodRelationshipRathanasara-0029',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0029.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0030', {
            url: '/secretOfGoodRelationshipRathanasara-0030',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0030.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0031', {
            url: '/secretOfGoodRelationshipRathanasara-0031',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0031.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0032', {
            url: '/secretOfGoodRelationshipRathanasara-0032',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0032.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0033', {
            url: '/secretOfGoodRelationshipRathanasara-0033',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0033.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0034', {
            url: '/secretOfGoodRelationshipRathanasara-0034',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0034.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0035', {
            url: '/secretOfGoodRelationshipRathanasara-0035',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0035.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0036', {
            url: '/secretOfGoodRelationshipRathanasara-0036',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0036.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0037', {
            url: '/secretOfGoodRelationshipRathanasara-0037',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0037.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0038', {
            url: '/secretOfGoodRelationshipRathanasara-0038',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0038.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0039', {
            url: '/secretOfGoodRelationshipRathanasara-0039',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0039.html'
        }) 
        .state('secretOfGoodRelationshipRathanasara.0040', {
            url: '/secretOfGoodRelationshipRathanasara-0040',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0040.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0041', {
            url: '/secretOfGoodRelationshipRathanasara-0041',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0041.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0042', {
            url: '/secretOfGoodRelationshipRathanasara-0042',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0042.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0043', {
            url: '/secretOfGoodRelationshipRathanasara-0043',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0043.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0044', {
            url: '/secretOfGoodRelationshipRathanasara-0044',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0044.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0045', {
            url: '/secretOfGoodRelationshipRathanasara-0045',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0045.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0046', {
            url: '/secretOfGoodRelationshipRathanasara-0046',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0046.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0047', {
            url: '/secretOfGoodRelationshipRathanasara-0047',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0047.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0048', {
            url: '/secretOfGoodRelationshipRathanasara-0048',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0048.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0049', {
            url: '/secretOfGoodRelationshipRathanasara-0049',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0049.html'
        }) 
        .state('secretOfGoodRelationshipRathanasara.0050', {
            url: '/secretOfGoodRelationshipRathanasara-0050',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0050.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0051', {
            url: '/secretOfGoodRelationshipRathanasara-0051',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0051.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0052', {
            url: '/secretOfGoodRelationshipRathanasara-0052',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0052.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0053', {
            url: '/secretOfGoodRelationshipRathanasara-0053',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0053.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0054', {
            url: '/secretOfGoodRelationshipRathanasara-0054',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0054.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0055', {
            url: '/secretOfGoodRelationshipRathanasara-0055',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0055.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0056', {
            url: '/secretOfGoodRelationshipRathanasara-0056',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0056.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0057', {
            url: '/secretOfGoodRelationshipRathanasara-0057',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0057.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0058', {
            url: '/secretOfGoodRelationshipRathanasara-0058',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0058.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0059', {
            url: '/secretOfGoodRelationshipRathanasara-0059',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0059.html'
        }) 
        .state('secretOfGoodRelationshipRathanasara.0060', {
            url: '/secretOfGoodRelationshipRathanasara-0060',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0060.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0061', {
            url: '/secretOfGoodRelationshipRathanasara-0061',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0061.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0062', {
            url: '/secretOfGoodRelationshipRathanasara-0062',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0062.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0063', {
            url: '/secretOfGoodRelationshipRathanasara-0063',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0063.html'
        })
        .state('secretOfGoodRelationshipRathanasara.0064', {
            url: '/secretOfGoodRelationshipRathanasara-0064',
            templateUrl: 'books/secretOfGoodRelationshipRathanasara/www/contents/secretOfGoodRelationshipRathanasara-0064.html'
        })

};


})();