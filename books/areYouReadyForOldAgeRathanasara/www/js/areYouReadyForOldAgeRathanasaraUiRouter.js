(function() {
    'use strict';
    
mainApp.config(areYouReadyForOldAgeRathanasaraUiRouter);

//The name cannot have dash character.
function areYouReadyForOldAgeRathanasaraUiRouter($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('areYouReadyForOldAgeRathanasara', {
            url: '/areYouReadyForOldAgeRathanasara',
            views: {
              // templateUrl will be loaded in the un-named view in index.html.
              '': {
                templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara.html',
                //controller : 'bookCtrl'
              },
              
              // templateUrl will be loaded in the un-named view in bookFolder.html 
              // which is embedded in areYouReadyForOldAgeRathanasara.html. The book-folder  
              // directive in areYouReadyForOldAgeRathanasara.html will load bookFolder.html.
              // 
              // '@home' - if in mainUiRouter.
              // '@areYouReadyForOldAgeRathanasara' - if in areYouReadyForOldAgeRathanasaraUiRouter.
              '@areYouReadyForOldAgeRathanasara': {
                templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0001.html',
                //controller : 'bookCtrl'  
              }
            }
        })

        // templateUrl in the states listed below will be loaded in the 
        // un-named view in bookFolder.html         
        .state('areYouReadyForOldAgeRathanasara.0001', {
            url: '/areYouReadyForOldAgeRathanasara-0001',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0001.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0002', {
            url: '/areYouReadyForOldAgeRathanasara-0002',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0002.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0003', {
            url: '/areYouReadyForOldAgeRathanasara-0003',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0003.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0004', {
            url: '/areYouReadyForOldAgeRathanasara-0004',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0004.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0005', {
            url: '/areYouReadyForOldAgeRathanasara-0005',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0005.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0006', {
            url: '/areYouReadyForOldAgeRathanasara-0006',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0006.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0007', {
            url: '/areYouReadyForOldAgeRathanasara-0007',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0007.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0008', {
            url: '/areYouReadyForOldAgeRathanasara-0008',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0008.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0009', {
            url: '/areYouReadyForOldAgeRathanasara-0009',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0009.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0010', {
            url: '/areYouReadyForOldAgeRathanasara-0010',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0010.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0011', {
            url: '/areYouReadyForOldAgeRathanasara-0011',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0011.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0012', {
            url: '/areYouReadyForOldAgeRathanasara-0012',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0012.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0013', {
            url: '/areYouReadyForOldAgeRathanasara-0013',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0013.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0014', {
            url: '/areYouReadyForOldAgeRathanasara-0014',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0014.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0015', {
            url: '/areYouReadyForOldAgeRathanasara-0015',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0015.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0016', {
            url: '/areYouReadyForOldAgeRathanasara-0016',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0016.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0017', {
            url: '/areYouReadyForOldAgeRathanasara-0017',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0017.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0018', {
            url: '/areYouReadyForOldAgeRathanasara-0018',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0012.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0019', {
            url: '/areYouReadyForOldAgeRathanasara-0019',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0019.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0020', {
            url: '/areYouReadyForOldAgeRathanasara-0020',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0020.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0021', {
            url: '/areYouReadyForOldAgeRathanasara-0021',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0021.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0022', {
            url: '/areYouReadyForOldAgeRathanasara-0022',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0022.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0023', {
            url: '/areYouReadyForOldAgeRathanasara-0023',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0023.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0024', {
            url: '/areYouReadyForOldAgeRathanasara-0024',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0024.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0025', {
            url: '/areYouReadyForOldAgeRathanasara-0025',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0025.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0026', {
            url: '/areYouReadyForOldAgeRathanasara-0026',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0026.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0027', {
            url: '/areYouReadyForOldAgeRathanasara-0027',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0027.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0028', {
            url: '/areYouReadyForOldAgeRathanasara-0028',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0028.html'
        })
        .state('areYouReadyForOldAgeRathanasara.0029', {
            url: '/areYouReadyForOldAgeRathanasara-0029',
            templateUrl: 'books/areYouReadyForOldAgeRathanasara/www/contents/areYouReadyForOldAgeRathanasara-0029.html'
        })
        
};


})();